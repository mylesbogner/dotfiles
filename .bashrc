export CLICOLOR=1

export PATH="$PATH:/Applications/Android Studio.app/sdk/tools:/Applications/Android Studio.app/sdk/platform-tools"

export PATH="/usr/local/bin:$PATH"

export ANDROID_HOME="/usr/local/lib/android-sdk"

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home

source ~/.bash_colors
if [ -f $(brew --prefix)/etc/bash_completion ]; then

. $(brew --prefix)/etc/bash_completion

export PS1=$txtcyn'[\W]'$txtylw'$(__git_ps1)'$txtrst' \$ '
fi

# Aliases
alias emacs=/Applications/Emacs.app/Contents/MacOS/Emacs
