; Myles' .emacs file.

; Add additional PATH statements.

;; Setup first for macOS brew paths.
(setenv "PATH" (concat "/usr/local/bin:" (getenv "PATH")))

; Inhibit welcome screen.
(setq inhibit-startup-message t)

; Do not make backup files.
(setq make-backup-files nil)

; Almost full height when starting Emacs on current laptop.
(when (window-system)
  (set-frame-height (selected-frame) 50))

; Word wrap on by default for text files.
; From:  http://www.emacswiki.org/emacs/VisualLineMode.
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)

; Add elisp load paths.  From http://www.emacswiki.org/emacs/LoadPath.
; Place a symbolic link to your downloaded elisp package subdirectories
; in .emacs.d.
(let ((default-directory "~/.emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))

; Start Emacs in tabbar mode (https://github.com/dholm/tabbar).
(require 'tabbar)
(tabbar-mode)

; Allow git to function properly when using Emacs Shell.
(setenv "EDITOR" "emacsclient")

; Turn on M-x send-invisible by default for hiding password in the clear for
; Emacs Shell.
(add-hook 'comint-output-filter-functions 'comint-watch-for-password-prompt)

; Allow up and down arrow keys on macOS to work in Emacs Shell.
(progn(require 'comint)
(define-key comint-mode-map (kbd "<up>") 'comint-previous-input)
(define-key comint-mode-map (kbd "<down>") 'comint-next-input))

; Disable erase buffer prompt.
(put 'erase-buffer 'disabled nil)

; ----------------------------------------
; Currently commented out.
; ----------------------------------------

; Autoload Javascript mode.
; (autoload 'javascript-mode "js-mode" nil t)
; (add-to-list 'auto-mode-alist '("\\.js\\'" . javascript-mode))

; Load Ruby mode:
; From: http://www.emacswiki.org/emacs/RubyMode
; (add-to-list 'auto-mode-alist
;              '("\\.\\(?:gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'"
;	       . ruby-mode))
; (add-to-list 'auto-mode-alist
;             '("\\(Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'"
;	      . ruby-mode))

; Load Markdown mode.
; From: http://jblevins.org/projects/markdown-mode/ (Emacs Mode)
; From: http://daringfireball.net/projects/markdown/ (Markdown)
; On MacOS, ensure to enter the command:  brew install markdown
; (autoload 'gfm-mode "markdown-mode"
;   "Major mode for editing Markdown files" t)
; (add-to-list 'auto-mode-alist '("\\.text\\'" . gfm-mode))
; (add-to-list 'auto-mode-alist '("\\.markdown\\'" . gfm-mode))
; (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))

; Enable Flyspell Mode by default for Markdown.
; (add-hook 'gfm-mode-hook 'flyspell-mode)

; Enable Emacs elisp package manager.
; (when (>= emacs-major-version 24)
;  (require 'package)
;  (package-initialize)
;  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
;)

; Set the screen background and foreground (default on macOS).
; (set-background-color "white")
; (set-foreground-color "black")

; ----------------------------------------
; Microsoft Windows specific items.
; ----------------------------------------

; Load Powershell Mode
; From: http://www.emacswiki.org/emacs/Powershell.el
; (load "Powershell.el")

; Improve slow performance on windows.
; From: http://zzamboni.org/blog/solving-hangs-in-emacs-on-windows/
; (setq w32-get-true-file-attributes nil)

; Don't show CTRL^Ms on Windows but don't remove them.
; (setq buffer-display-table (make-display-table))
; (aset buffer-display-table ?\^M [])

